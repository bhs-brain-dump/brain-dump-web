import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService } from './firebase.service';
import { User, auth } from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public authStateChecked: boolean = false;
  public user: User;

  constructor(
    public router: Router,
    public firebaseService: FirebaseService
  ) { }

  ngOnInit() {
    auth().onAuthStateChanged(user => {
      this.authStateChecked = true;
      this.user = user;
    })
  }

  signOut() {
    auth().signOut();
  }

}
