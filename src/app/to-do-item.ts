export interface ToDoItem {
  id: string;
  task: string;
  details: string;
  creationDate: Date;
  archived: boolean;
}
