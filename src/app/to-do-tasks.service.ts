import { Injectable } from '@angular/core';
import { firestore, User, auth } from 'firebase/app';
import { ToDoItem } from './to-do-item';

@Injectable({
  providedIn: 'root'
})
export class ToDoTasksService {

  public user: User;
  public toDoItems: ToDoItem[] = [];

  constructor() {
    auth().onAuthStateChanged(user => {
      if (user != null) {
        this.user = user;
        this.registerToDoUpdates(user);
      }
    });
  }

  registerToDoUpdates(user: User) {
    const inst = this;
    firestore().collection('users').doc(user.uid).collection('tasks').onSnapshot({
      next(snapshot: firestore.QuerySnapshot) {
        inst.toDoItems = snapshot.docs
          .map(x => {
            let y = x.data() as ToDoItem;
            y.id = x.id;
            return y;
          })
          .sort((a, b) => {
            if (a.creationDate > b.creationDate) {
              return -1;
            } else if (a.creationDate < b.creationDate) {
              return 1;
            } else {
              return 0;
            }
          });
      }
    });
  }

  addToDoItem(task: string, details: string) {
    firestore().collection('users').doc(this.user.uid).collection('tasks').add({
      task,
      details,
      creation_date: Date.now()
    });
  }

  setArchivedToDoItem(id: string, archived: boolean) {
    firestore().collection('users').doc(this.user.uid).collection('tasks').doc(id).update({ archived: true });
  }

}
