import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as firebaseui from 'firebaseui';
import { auth } from 'firebase';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
    auth().onAuthStateChanged(user => {
      if (user) {
        this.router.navigate(['']);
      }
    });

    let ui = new firebaseui.auth.AuthUI(auth());
    ui.start('#firebaseui-auth-container', {
      signInSuccessUrl: `${environment.baseUrl}/`,
      signInOptions: [
        auth.GoogleAuthProvider.PROVIDER_ID,
        auth.FacebookAuthProvider.PROVIDER_ID,
        auth.TwitterAuthProvider.PROVIDER_ID,
        auth.GithubAuthProvider.PROVIDER_ID,
        auth.EmailAuthProvider.PROVIDER_ID,
        auth.PhoneAuthProvider.PROVIDER_ID
      ]
    });
  }

}
