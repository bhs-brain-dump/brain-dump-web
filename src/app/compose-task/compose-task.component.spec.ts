import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComposeTaskComponent } from './compose-task.component';

describe('ComposeTaskComponent', () => {
  let component: ComposeTaskComponent;
  let fixture: ComponentFixture<ComposeTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComposeTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComposeTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
