import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-compose-task',
  templateUrl: './compose-task.component.html',
  styleUrls: ['./compose-task.component.scss']
})
export class ComposeTaskComponent implements OnInit {

  task: string = null;
  details: string = null;

  constructor(public dialogRef: MatDialogRef<ComposeTaskComponent>) { }

  ngOnInit() { }

  cancel() {
    this.dialogRef.close(null);
  }

  submitTask() {
    this.dialogRef.close({
      task: this.task,
      details: this.details
    });
  }

}
