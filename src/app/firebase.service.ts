import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor() {
    firebase.initializeApp(environment.firebase);
    firebase.firestore().settings(environment.firestore);
  }

}
