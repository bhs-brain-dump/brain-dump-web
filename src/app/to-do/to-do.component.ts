import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { User, auth } from 'firebase';
import { ToDoTasksService } from '../to-do-tasks.service';
import { ComposeTaskComponent } from '../compose-task/compose-task.component';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.scss']
})
export class ToDoComponent implements OnInit {

  public user?: User = null;

  constructor(
    public router: Router,
    public dialog: MatDialog,
    public toDoTasksService: ToDoTasksService
  ) { }

  ngOnInit() {
    auth().onAuthStateChanged(user => {
      if (!user) {
        this.router.navigate(['login']);
      } else {
        this.user = user;
      }
    })
  }

  composeTask() {
    this.dialog.open(ComposeTaskComponent).afterClosed().subscribe((result: any) => {
      if (result && result.task != null) {
        this.toDoTasksService.addToDoItem(result.task, result.details);
      }
    });
  }

}
